import asyncio
from PlayerManager import PlayerManager
from ServersManager import ServersManager
from ServerListProxy import ServerListProxy
from AuthProxy import AuthProxy
from APIRequest import APIRequest
import logging
import ipaddress
import signal
import sys

class BF2MasterServer:
    def __init__(self, bindTo="127.0.0.1", packetFilter=ipaddress.ip_network("127.0.0.1/32")):
        self.authProxy = None
        self.serverListProxy = None
        self.serversManager = None
        self.playerManager = None
        self.bindTo = bindTo
        self.packetFilter = packetFilter
        self.shutdown_event = asyncio.Event()
        self.loop = asyncio.get_event_loop()

    async def start(self):
        # Configure graceful shutdown only on non-Windows platforms
        if sys.platform != "win32":
            signals = (
                signal.SIGINT,
                signal.SIGTERM,
                signal.SIGHUP,
            )  # Include SIGHUP on Unix/Linux
            for sig in signals:
                self.loop.add_signal_handler(
                    sig, lambda sig=sig: asyncio.create_task(self.signal_handler())
                )

        self.serversManager = ServersManager()
        self.playerManager = PlayerManager()


        logging.info("Starting BF2 master server bound to %s with subnet %s" % (self.bindTo, self.packetFilter))

        transport, self.authProxy = await self.loop.create_datagram_endpoint(
            lambda: AuthProxy(
                playerManager=self.playerManager,
                serversManager=self.serversManager,
                localIP=self.bindTo,  # Changed here
                packetFilter=self.packetFilter,
            ),
            local_addr=(self.bindTo, 29910),
        )

        transport, self.serverListProxy = await self.loop.create_datagram_endpoint(
            lambda: ServerListProxy(
                serversManager=self.serversManager,
                localIP=self.bindTo,  # Changed here
                packetFilter=self.packetFilter,
            ),
            local_addr=(self.bindTo, 27900),
        )

        # Wait for shutdown signal
        await self.shutdown_event.wait()
        await self.shutdown()

    async def signal_handler(self):
        logging.info("Received shutdown signal")
        self.shutdown_event.set()

    async def shutdown(self):
        logging.info("Shutting down...")
        logging.info("Closing API session...")
        await APIRequest.close_session()

        logging.info("Stopping ServersManager...")
        if self.serversManager:
            self.serversManager.stop()

        logging.info("Closing transports...")
        if self.authProxy and hasattr(self.authProxy, "UDPTransport"):
            self.authProxy.UDPTransport.close()
        if self.serverListProxy and hasattr(self.serverListProxy, "UDPTransport"):
            self.serverListProxy.UDPTransport.close()

        logging.info("Cancelling tasks...")
        tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
        [task.cancel() for task in tasks]
        await asyncio.gather(*tasks, return_exceptions=True)

        logging.info("Stopping event loop...")
        self.loop.stop()

        logging.info("Shutdown complete")


# Main execution
if __name__ == "__main__":
    server = BF2MasterServer()
    try:
        asyncio.run(server.start())
    except KeyboardInterrupt:
        logging.info("BF2 Master Server interrupted by user.")
    finally:
        logging.info("BF2 Master Server has been shut down.")
        asyncio.get_event_loop().stop()
        asyncio.get_event_loop().close()