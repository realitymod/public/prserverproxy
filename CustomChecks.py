# Custom checks on IP(Block VPN?) or Hash(Shared banlist between communities?)
# Return True on both to ALLOW.
# Return False on at least one to block.
# Whitelist takes priority

#### APIRequest async example:
# req = APIRequest(URL, timeout=5, type="post") # or type="get"
# req.data = data # python object that can be serialized to json, only in POST
# (status, response) = await req.send()
# if status == "Success":
#     data = json.loads(response)
# elif status == "HTTPError":
#     HTTPStatus = response.status
# else: # Non-HTTP error (timed out or unknown exception)
#     pass

import APIRequest
async def IPCheck(IP: str) -> bool:
    return True

async def HashCheck(hash: str) -> bool:
    return True
