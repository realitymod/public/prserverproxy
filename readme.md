Requirements: <br>
-Python 3.7+<br>
-aiohttp (`pip install aiohttp` or `python -m pip install aiohttp`)
<br>
<br>
Usage: <br>
Make sure `license.key` sits in the Game Server's mods directory <br>
edit `realityconfig_admin.py` in each server's mods/pr/python/game folder for server-specific settings<br>
run `python main.py` <br>
<br>
WHITELIST:<br>
Server entrance whitelist can be found in `whitelist.txt` for Account IDs (aka hash) and `whitelistnames.txt` for profile names.<br>
One entry per line, no comments, case insensitive<br>
<br>
Can be safely restarted without requiring any Game Server restart.<br>
