import asyncio
from typing import Any, Dict, Optional
from APIRequest import APIRequest
import time
import io
# Stores information about servers running on this machine and their properties.


URL_SERVERINFO = "https://servers.realitymod.com/api/ServerInfo"
class Server:
    def __init__(self, port):
        self.port = port
        self.pythonAddr = None
        self.lastKeepAlive = time.time()
        self.properties = {}
        self.players = []
        #self.key = None
        self.licenseKey = None
        self.serverIp = None
        self.configuration = {}
        self.ready = False

        #print("Added new server, port %s" % port)


    def __str__(self):
        s = "%s (Game: %s, Query: %s) | " % (self.getServerName(), self.getGamePort(), self.port)
        #s += "Last report %ss ago | " % round(time.time() - self.lastKeepAlive)
        if self.licenseKey is None:
            s += "Waiting for server configs..."
        if self.licenseKey is not None and len(self.licenseKey) == 0:
            s += "ERROR: error parsing License Key"


        return s

    def setPythonAddr(self, pythonAddr) -> None:
        self.pythonAddr = pythonAddr

    def setLicenseKey(self, licenseKey) -> None:
        self.licenseKey = licenseKey

    def setServerIp(self, serverIp) -> None:
        self.serverIp = serverIp

    def setPlayers(self, players):
        self.players = players
        self.keepAlive()

    # int32 randomly generated every server LAUNCH. Could be used to detect when server restarted.
    # def setKey(self, key) -> None:
    #     self.key = key

    def updateConfiguration(self, newconfig) -> None:
        self.configuration.update(newconfig)
        self.keepAlive()

    def getConfiguration(self) -> Dict[str, Any]:
        return self.configuration

    def getServerName(self) -> Optional[str]:
        return self.properties.get("hostname", "Unknown server name")

    def getGamePort(self) -> Optional[str]:
        return self.properties.get("hostport", "Unknown game port")

    def keepAlive(self) -> None:
        self.lastKeepAlive = time.time()

    def getVersion(self):
        return self.configuration.get("version", "")

    def getMod(self):
        return self.configuration.get("mod", "")

    def parseInfo(self, data: str) -> None:
        #print(data.replace("\x00", "\\x00").replace("\x01", "\\x01").replace("\x02", "\\x02"))

        self.keepAlive()

        stream = io.StringIO(data)

        first = stream.read(1)
        while first != "\x00" and first != "":
            stream.seek(stream.tell()-1)
            key = _streamcut(stream, data, "\x00")
            val = _streamcut(stream, data, "\x00")
            self.properties[key] = val
            first = stream.read(1)

        if self.properties.get("numplayers", 0) == 0:
            self.players = []


        # "localipX" are lists of interfaces the server can bind on. 
        # Some people want this private as this can contain public IPs that are not behind stuff
        for key in ("localip%s" % x for x in range(10)):
            if key not in self.properties: break
            del self.properties[key]

        self.keepAlive()
        self.ready = True

        # This is all pointless. Linux doesn't do it, only windows.
        # stream.read(1)
        #
        # playercount = stream.read(1)
        # if len(playercount) == 0:
        #     self.ready = True
        #     return
        #
        # playerdatacolumndefs = streamcut(stream, data, "\x00\x00").split("\x00")
        #
        # for i in range(ord(playercount)):
        #     player = {}
        #     for columnname in playerdatacolumndefs:
        #         player[columnname] = streamcut(stream, data, "\x00")
        #
        #     self.players.append(player)
        # stream.read(1)
        #
        #
        # teamcount = ord(stream.read(1))
        # teamdatacolumndefs = streamcut(stream, data, "\x00\x00").split("\x00")
        #
        #
        # for i in range(teamcount):
        #     team = {}
        #     for columnname in teamdatacolumndefs:
        #         team[columnname] = streamcut(stream, data, "\x00")
        #
        #     self.teams.append(team)

class ServersManager:
    def __init__(self):
        self.servers = {}
        self.running = True
        self.semaphore = asyncio.Semaphore(10)  # Limit to 10 concurrent requests
        asyncio.create_task(self._updateMasterServerLoop())
        asyncio.create_task(self._printStatus())

    async def _printStatus(self):
        while self.running:
            await asyncio.sleep(20)
            now = time.time()
            for server in filter(lambda server: server.lastKeepAlive + 300 < now, list(self.servers.values())):
                del self.servers[server.port]

            if len(self.servers.values()) != 0:
                print(time.strftime("[%Y-%m-%d %H:%M:%S]"))
                for server in self.servers.values():
                    print(str(server))

    async def _updateMasterServerLoop(self):
        while self.running:
            await asyncio.sleep(30)

            now = time.time()
            update_tasks = []
            for server in filter(lambda server: 
                                 server.lastKeepAlive + 80 > now and # Recent keep alive
                                 server.licenseKey is not None and # We have license key
                                 server.ready and # Received a serverinfo packet
                                 len(server.properties.get("hostport", "")) > 0, self.servers.values()): # hostport property is ready (first serverinfo packets are missing data)
                update_tasks.append(self._update_server(server))

            await asyncio.gather(*update_tasks)

    async def _update_server(self, server):
        async with self.semaphore:
            req = APIRequest(URL_SERVERINFO, server.licenseKey, timeout=15)
            req.data = self.getServerInfo(server)
            await req.send()

    def stop(self):
        self.running = False

    def getServerInfo(self, server) -> Dict[str, Any]:
        return {
            "ServerIp": server.serverIp,
            "QueryPort": server.port,
            "Properties": server.properties, # Vanilla properties only
            "Config": server.configuration,
            "Players": server.players,
        }

    def getOrNew(self, port) -> Server:
        port = int(port)
        if port not in self.servers:
            self.servers[port] = Server(port)

        return self.servers[port]




def _streamcut(stream, data, find):
    offset = stream.tell()
    target = data.find(find, offset)
    if target == -1:
        raise Exception("Error parsing server info. Received: %s" % data
                        .replace("\x00", "\\x00")
                        .replace("\x01", "\\x01")
                        .replace("\x02", "\\x02"))

    size = target - offset
    ret = stream.read(size)
    stream.read(len(find))
    return ret