import asyncio
import collections
import json
import re
from typing import Union
from APIRequest import APIRequest
import CustomChecks
import logging
import traceback

URL_PROFILEDATA = "https://servers.realitymod.com/api/auth/session"


class AuthProxy(asyncio.DatagramProtocol):
    # Game server can retry the UDP request packet multiple times. Ignore recent duplicates, our TCP is reliable.
    recentRequests = collections.deque([""] * 16)


    def __init__(self, playerManager, serversManager, localIP, packetFilter):
        self.playerManager = playerManager
        self.serversManager = serversManager
        self.localIP = localIP
        self.packetFilter = packetFilter

    def connection_made(self, transport):
        self.UDPTransport = transport
        logging.info("Auth Proxy bound")

    def datagram_received(self, data, addr):
        data = str(data, encoding="utf-8")

        # Game Server will request player data info from the proxy
        # This is done from Python and the address (port) will be different than the server's gamepsy port.
        if data.startswith("\\playerdata\\\\"):
            hash = data[len("\\playerdata\\\\"):]
            self.UDPTransport.sendto(self.playerManager.getPlayerData(hash), addr)
            return


        if data.startswith("\\addwhitelist\\\\"):
            hash = data[len("\\addwhitelist\\\\"):]
            self.playerManager.addToWhitelist(hash)
            return

        # Configuration packet. This will come from python (and will use a random source port). We cannot rely on the addr.
        # configuration \\ port \ serverip \ LicenseKey \ data
        elif data.startswith("\\configuration\\\\"):
            split = data[len("\\configuration\\\\"):].split("\\")

            port = split[0]  # The sv.gamespyport is the identity of the server.
            serverIp = split[1] # The sv.serverIp
            licenseKey = split[2]
            configuration = json.loads(split[3])

            if len(licenseKey) == 0:
                logging.fatal("Error reading license key in server with queryport %s. Please make sure its at mods/license.key" % port)
                return

            server = self.serversManager.getOrNew(port)
            server.setLicenseKey(licenseKey)
            server.setServerIp(serverIp)
            server.updateConfiguration(configuration)
            server.setPythonAddr(addr)
            return

        elif  data.startswith("\\playerlist\\\\"):
            port, playersjson = data[len("\\playerlist\\\\"):].split("\\", 1)
            server = self.serversManager.getOrNew(port)
            server.setPlayers(json.loads(playersjson))
            server.setPythonAddr(addr)
            return

        data = xorgamepsy(data)
        if data.startswith("\\auth\\\\"):
            try:
                regex = r"^\\auth\\\\pid\\(\d*)\\ch\\(.*)\\resp\\(.*)\\ip\\(-?\d*)\\skey\\(\d*)\\reqproof\\1\\$"
                res = re.match(regex, data)
                if res is None:
                    logging.error("WARNING: could not parse auth request from server: %s " % data)
                    return

                request = {"ch": res.group(2),
                           "resp": res.group(3),
                           "ip": res.group(4),
                           "skey": res.group(5),
                           "reqproof": "1"
                           }


                asyncio.create_task(self.newDataRequest(request, addr))
                return
            except:
                logging.error(str(traceback.format_exc()))
                return
        # Player disconnect
        elif data.startswith("\\disc\\"):
            pass
        # Keep alive
        elif data.startswith("\\ka\\"):
            pass
        else:
            logging.error("unknown auth message %s" % data)
        return

    async def newDataRequest(self, request, replyaddr):
        gamespyport = replyaddr[1]
        server = self.serversManager.getOrNew(gamespyport)
        clientResponse = request["resp"]
        responseregex = "^[0-9a-f]{72}$"
        res = re.match(responseregex, clientResponse)
        if res is None:
            logging.error("Client response is not in protocol! %s" % clientResponse)
            return self.sendNotOkay(request, replyaddr)

        if server.licenseKey is None:
            logging.error("Denied a player (%s): server not ready" % clientResponse[:32])
            return self.sendNotOkay(request, replyaddr)

        keyHash = clientResponse[:32]
        requestOut = {
            "ip": request["ip"],
            "servertoken": request["ch"],
            "keyhash": keyHash,
            "clienttoken": clientResponse[32:40],
            "validationtoken": clientResponse[40:72],
            "version": server.getVersion(),
            "mod": server.getMod()
        }


        if clientResponse in self.recentRequests:
            # This is a duplicate (retrying) UDP packet. Don't initiate an extra HTTP request.
            # Response is based on a unique challenge randomized every join, so its unique.
            return
        self.recentRequests.popleft()
        self.recentRequests.append(clientResponse)

        req = APIRequest(URL_PROFILEDATA, timeout=6)
        req.data = requestOut
        ip = int(request["ip"])
        tasks = asyncio.gather(
            CustomChecks.IPCheck("%s.%s.%s.%s" % (ip % 256, (ip >> 8) % 256, (ip >> 16) % 256, (ip >> 24) % 256)),
            CustomChecks.HashCheck(keyHash),
        )
        (status, response) = await req.send()
        try:
            IPRes, HashRes = await asyncio.wait_for(tasks, timeout=6)
            customCheckOkay = IPRes and HashRes
        except:
            logging.error("Custom checks timed out or errored")
            customCheckOkay = True

        if status == "Success":
            data = json.loads(response)
            accept, error = self.playerManager.addPlayerAndCheckPoints(data, server.getConfiguration(), customCheckOkay)
            if accept:
                self.sendOkay(request, replyaddr)
            else:
                # Reply to BF2 vanilla gamespy protocol
                self.sendNotOkay(request, replyaddr)
                # Send to PR Python and let admins know
                self.sendJoinDeniedReport(data, server, error)
        elif not customCheckOkay:
            self.sendNotOkay(request, replyaddr)
            logging.warning("Server Error, but IP/Hash are custom blocked. Join denied. Request: %s" % request)
        elif status == "HTTPError":
            if response.status == 404:
                self.sendNotOkay(request, replyaddr)
                logging.info("Server returned 404, Session not found, Join denied. Request: %s" % request)
            else:
                self.playerManager.addPlayerMSError(keyHash)
                logging.warning("Server returned HTTP code %s. Join not denied. Request: %s" % (response.status, request))
                logging.warning("content: %s" % await response.read())
        else:
            self.playerManager.addPlayerMSError(keyHash)
            logging.warning("Error connecting to masterserver. Join not denied. Request: %s" % request)


    def sendJoinDeniedReport(self, data, server, reason="TODO"):
        if server.pythonAddr is None:
            return

        data["denyReason"] = reason
        self.UDPTransport.sendto(b"\\denied\\\\" + bytes(json.dumps(data), encoding="utf-8"), server.pythonAddr)

    def sendNotOkay(self, request, addr):
        out = bytes(xorgamepsy("\\unok\\\\cd\\%s\\skey\\%s\\errmsg\\%s" % (request["resp"][:32], request["skey"], "Invalid Session")) , encoding="utf-8")
        self.UDPTransport.sendto(out, addr)


    def sendOkay(self, request, addr):
        out = bytes(xorgamepsy("\\uok\\\\cd\\%s\\skey\\%s" % (request["resp"][:32], request["skey"])), encoding="utf-8")
        self.UDPTransport.sendto(out, addr)



def xorgamepsy(s : Union[str, bytes]) -> str:
    if isinstance(s, str):
        s = bytes(s, encoding="utf-8")

    out = ""
    for i in range(len(s)):
        out += chr(s[i] ^ ord("gamespy"[i % 7]))

    return out
