import aiohttp
import logging
import traceback

from aiohttp import ClientConnectionError

class APIRequest:
    _session = None

    @classmethod
    async def get_session(cls):
        if cls._session is None:
            cls._session = aiohttp.ClientSession()
        return cls._session

    @classmethod
    async def close_session(cls):
        if cls._session:
            await cls._session.close()
            cls._session = None

    def __init__(self, url, licenseKey=None, timeout=5, type="post"):
        self.url = url
        self.licenseKey = licenseKey
        self.timeout = timeout
        self.data = None
        self.type = type

    async def send(self):
        try:
            if self.licenseKey is not None:
                headers = {'PR-license-key': self.licenseKey}
            else:
                headers = None

            session = await self.get_session()
            async with session.request(self.type.lower(),
                                         self.url,
                                         json=self.data,
                                         headers=headers,
                                         timeout=aiohttp.ClientTimeout(total=self.timeout)) as response:
                if response.status != 200:
                    logging.warning("HTTP code %s, content: %s from %s" % (response.status, await response.read(), self.url))
                    return "HTTPError", response
                return "Success", await response.read()

        except ClientConnectionError as e:
            logging.warning("Could not connect to %s: %s" % (self.url, str(e)))
            return "Error", e
        except Exception as e:
            exc = traceback.format_exc()
            logging.error(str(exc))
            return "Error", e
