import asyncio
import json
import time
from typing import Any, Dict, Optional, Union
import os.path

# Stores results from player data queries and forwards them to Game Server trough Game-Python
# Practically Name Verification Protocol MK2.
class PlayerManager:
    def __init__(self):
        # hash -> player data dict
        self.knownPlayers = {}
        self.whitelist = set()
        self.whitelistNames = set()

        if os.path.exists("whitelist.txt"):
            f = open("whitelist.txt", "rb")
            for line in f.read().split(b"\n"):
                self.whitelist.add(line.strip().lower())
            f.close()

        if os.path.exists("whitelistnames.txt"):
            f = open("whitelistnames.txt", "rb")
            for line in f.read().split(b"\n"):
                self.whitelistNames.add(line.strip().lower())
            f.close()

        asyncio.create_task(self._deleteOld())

    def addToWhitelist(self, hash : str):
        hash = hash.strip().lower()
        if hash in self.whitelist:
            return

        self.whitelist.add(hash)
        f = open("whitelist.txt", "ab")
        f.write(hash.encode("utf-8") + b"\n")
        f.close()


    def isHashWhitelisted(self, hash):
        if hash.__class__ is str:
            hash = hash.encode("utf-8")
        return hash.lower() in self.whitelist

    def isNameWhitelisted(self, name):
        if name.__class__ is str:
            name = name.encode("utf-8")
        return name.lower() in self.whitelistNames

    def getPlayerData(self, hash : str) -> bytes:
        if hash not in self.knownPlayers:
            return b"\\playerdata\\\\" + bytes(json.dumps({
                "keyHash": hash,
                "known": False,
            }), encoding="utf-8")

        player = self.knownPlayers[hash]

        return b"\\playerdata\\\\" + bytes(json.dumps(player.__dict__), encoding="utf-8")


    async def _deleteOld(self):
        while True:
            await asyncio.sleep(120)
            for player in list(self.knownPlayers.values()):
                if time.time() - 120 > player.dataTime:
                    del self.knownPlayers[player.keyHash]


    def isWhiteListed(self, data):
        return self.isHashWhitelisted(data["keyHash"]) or self.isNameWhitelisted(data["name"])

    """
    Allows the player to join if MS times out or returns unexpected errors
    """
    def addPlayerMSError(self, keyHash):
        # do not overwrite
        if keyHash in self.knownPlayers:
            return

        player = self._newPlayer({"keyHash": keyHash})
        player.unexpectedErrorVerifying = True

    """
    Check if player has the minimum required to join the server, and add his data to the list
    """
    def addPlayerAndCheckPoints(self, data: Dict[str, Any], serverconfig: Dict[str, Any], customCheckOkay=True) -> (bool, str):
        isWhiteListed = self.isWhiteListed(data)
        self._newPlayer(data, isWhiteListed = isWhiteListed)
        if isWhiteListed:
            return True, "Whitelisted"

        if not customCheckOkay:
            return False, "Custom checks failed"

        if data.get("vacBanned", False) and not serverconfig.get("ec_allowVacBanned", True):
            return False, "Vac Banned"

        required = serverconfig.get("ec_minimumTrust", 999)
        if data["trustLevel"] < required:
            return False, "Low trust. has: %s, required %s" % (data["trustLevel"], required)

        return True, ""

    def _newPlayer(self, data, isWhiteListed = False):
        assert ("keyHash" in data)

        p = self._Player(data, isWhiteListed)
        self.knownPlayers[data["keyHash"]] = p
        return p

    class _Player:
        def __init__(self, data, isWhiteListed = False):
            self.known = True
            self.whitelisted = isWhiteListed
            self.unexpectedErrorVerifying = False
            self.dataTime = time.time()
            self.__dict__.update(data)
